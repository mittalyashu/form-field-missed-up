const fs = require('fs')
const path = require('path')

const p = path.join(
	path.dirname(process.mainModule.filename),
	'data',
	'cart.json'
)

module.exports = class Cart {
	static addProduct(id, productPrice) {
		// Fetch the previous cart
		fs.readFile(p, (err, fileContent) => {
			let cart = { products: [], totalPrice: 0 }
			if (!err) {
				cart.JSON.parse(fileContent)
			}
			// Analyse the cart => Find existing product
			const existingProductIndex = cart.products.findIndex(
				prod => prod.id === id
			)
			const existingProduct = cart.products[existingProductIndex]
			let updatedProduct
			if (existingProduct) {
				updatedProduct = { ...existingProduct }
				updatedProduct.qyt = updatedProduct.qyt + 1
				cart.products = [...cart.products]
				cart.products[existingProductIndex] = updatedProduct
			} else {
				updatedProduct = { id: id, qyt: 1 }
			}
			cart.totalPrice = cart.totalPrice + productPrice
			cart.products = [...cart.products, updatedProduct]
			fs.writeFile(p, JSON.stringify(cart), err => {
        console.log(err);
      })
		})
	}
}
